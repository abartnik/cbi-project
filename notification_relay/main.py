import os
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import select
from datetime import datetime
import requests
import json


DB_USER = os.environ['XNAT_DATASOURCE_USERNAME']
DB_PASSWD = os.environ['XNAT_DATASOURCE_PASSWORD']
DB_URL = os.environ['XNAT_DATASOURCE_HOST']

connection = psycopg2.connect(
    host=DB_URL,
    database="xnat",
    user=DB_USER,
    password=DB_PASSWD)

connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
cur = connection.cursor()

cur.execute("""
CREATE OR REPLACE FUNCTION update_container_notify()
RETURNS trigger AS
$BODY$

BEGIN
PERFORM pg_notify('update_container', row_to_json(NEW)::text);
RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION update_container_notify()
OWNER TO xnat;

DROP TRIGGER IF EXISTS update_container_trigger on xhbm_container_entity ;
CREATE TRIGGER update_container_trigger
AFTER UPDATE
ON xhbm_container_entity
FOR EACH ROW
EXECUTE PROCEDURE update_container_notify();
""")

cur.execute("LISTEN update_container;")
print("Waiting for notifications on channel 'update_container'")

while True:
    
    ## Keep the connection to the DB alive every ten seconds
    if select.select([connection], [], [], 10) == ([], [], []):
        pass
        
    ## Handle a new notification from the XNAT container DB
    else:
        connection.poll()
        while connection.notifies:
            notify = connection.notifies.pop(0)
            payload = json.loads(notify.payload)
            
            body = {'id': payload['id'],
                    'created': payload['created'],
                    'timestamp': payload['timestamp'],
                    'command_id': payload['command_id'],
                    'docker_image': payload['docker_image'],
                    'project': payload['project'],
                    'status': payload['status'],
                    'status_time': payload['status_time'],
                    'workflow_id': payload['workflow_id'],
                    'wrapper_id': payload['wrapper_id']}
            
            response = requests.get("http://pipeline_engine:8100/containers/")
            containers = response.json()['containers']
            if len(containers) > 0:
                container_ids = [ container['id'] for container in containers ]
                if body['id'] in container_ids:
                    response = requests.put(f"http://pipeline_engine:8100/containers/{body['id']}", 
                                                json=body)
                else:
                    response = requests.post(f"http://pipeline_engine:8100/containers/{body['id']}", json=body)

            else:
                response = requests.post(f"http://pipeline_engine:8100/containers/{body['id']}", json=body)
