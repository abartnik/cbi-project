# Notification Relay

---------------------

## Background
Earlier versions of CRANIA built a pipeline manager directly into the [XNAT Client](../xnat/), which would be launched with each exam as it was archived and polled the XNAT server for the runtime status of container processes. Some processes run for hours, and disconnects from the XNAT server while polling would completely break the pipeline without any way of recovering or resuming. Exams could also only be processed in series, meaning some exams had to wait in the prearchive for hours while a pipeline finished on a prior scan.

XNAT uses [PostgreSQL](https://www.postgresql.org/) as its relational database backend. XNAT's choice of RDBMS was extremely fortunate for the development of the CRANIA platform, as postgres natively supports [table update notifications](https://www.postgresql.org/docs/9.2/sql-notify.html) as plaintext or JSON. This means that we can inject an UPDATE/NOTIFY trigger into the table that holds XNAT Container Service container status information and automatically send an update payload to a listening client. This allowed us to handle container processes in realtime and in parallel without needing to poll the server.

## How it works
1. A connection from the psql client is made to the XNAT postgres database
2. The notification relay system adds an UPDATE/NOTIFY query to the container status table in the XNAT postgres database, dropping it first if it exists to avoid errors
3. Whenever an update is made to the container status table, a payload is sent to the listening client in JSON that encodes the container ID and runtime status (e.g. "Running," "Failed," "Complete," etc.)
4. The notification relay makes POST/PUT requests to the [Pipeline Engine API](../pipeline_engine/) for each container status udate
5. The Pipeline Engine handles the logic for coordinating pipelines and launching containers, the notification relay simply provides the Pipeline Engine the ability to work with the XNAT Container Service in realtime.
