#!/bin/bash

version=$1

docker build xnat/ -t registry.bnac.net/crania/crania:$version
docker push registry.bnac.net/crania/crania:$version

docker build pipeline_engine/ -t registry.bnac.net/crania/pipeline_engine:$version
docker push registry.bnac.net/crania/pipeline_engine:$version

docker build notification_relay/ -t registry.bnac.net/crania/notification_relay:$version
docker push registry.bnac.net/crania/notification_relay:$version
