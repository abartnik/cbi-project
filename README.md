# Centralized Repository of Advanced Neuroinformatics Applications (CRANIA) - A CBI Neuroinformatics Platform

The CRANIA project is an integrated neuroinformatics platform built on XNAT in an effort to centralize neuroimaging scans and real-time, automated analytics to facilitate translational research. As of now, the project consists of three major parts: the XNAT server, an MRI/analysis ontology, and the CRANIA python application that controls the automated process.

## System architecture
![System Architecture](system_architecture.png)

## XNAT Server
The XNAT dev server is accessible at [http:/donut.bnac.net](http://donut.bnac.net). The production server is available at [https://xnat.cbi.bnac.net](https://xnat.cbi.bnac.net).
The XNAT system is organized at the highest level into projects, each with its own automated analysis pipeline configurations. The analyses are all run via docker containers, using XNAT’s container service runtime, which allows us to read data from XNAT and add new data from analysis outputs in real-time.
For more information, see the [XNAT Docs](https://wiki.nxat.org/documentation). 

## XNAT Python API (CRANIA)
XNAT provides a Python API for hooking into the XNAT REST API. This was used to create the [CRANIA XNAT Client (CXC)](./xnat/), which listens for incoming scans as they are added to the prearchive on a five second interval (the default refresh interval for the XNAT database). When a scan is fully received into prearchive, the CXC classifies its type and sorts it into a specified XNAT project. Major preprocessing analyses (dcm2niix, fmriprep, freesurfer, etc.) are run automatically within XNAT upon a scan’s archiving, and an additional analysis may be chained to the output of that. More complex pipelines and storage of analysis results are handled by the [Pipeline Engine API](./pipeline_engine/), which is tied directly into the XNAT PostgreSQL database via the [notification relay](./notification_relay/).
