FROM ubuntu:18.04

RUN apt update; apt install -y python3 python3-pip

RUN mkdir /xnat
WORKDIR /xnat

COPY requirements.txt /xnat
RUN pip3 install -r /xnat/requirements.txt

COPY xnat /xnat
