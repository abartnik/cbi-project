from enum import Enum


class AnalysisType(Enum):
    """
    Enum class used to map predicted classification code to XNAT project
    based on analysis type
    T2star = 0
    DWI = 1
    FLAIR = 2
    fmap = 3
    rsfmri = 4
    FSE = 5
    MPRAGE = 6
    PD = 7
    SPGR = 8
    """
    DWI = 677      #MRIO_0000677 (also potentially 350?)
    FLAIR = 392    #MRIO_0000392
    fmap = 2       #MRIO_0000???
    rsfmri = 354   #MRIO_0000686 (just functional; no resting state fMRI seq. class yet)
    FSE = 388      #MRIO_0000388
    MPRAGE = 678   #MRIO_0000678
    SPGR = 681     #MRIO_0000681
