**TODO**:
 - Train and deploy XGBoost MRI classifier
 - Deploy MRIO in application
 - Adapt Mackenzie's SPARQL
 - Compile dictionary of pipeline names mapped to analysis terms in MRIO
 - Send exam ID and analysis pipeline as JSON/dict to Pipeline Engine API
