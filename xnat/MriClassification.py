import os
import sys
sys.path.append('.')
import zipfile
import json
import shutil
from AnalysisType import AnalysisType
from joblib import load
from nipype.interfaces.dcm2nii import Dcm2niix
from glob import glob
from scan_classifier.scan_classifier import model, read_dcm_header, format_input_for_model, predict_scan_type


class MriClassification:
    """
    Loads the external MRI analysis type classifier and predicts the analysis
    to be performed on a scan using TE, TR, TI, and scanning sequence;
    """

    def __init__(self, scan):

        self.scan = scan
        # self.clf = load('mri_tree.joblib')

    def predict_analysis_type(self):
        """
        Selects a scan from XNAT and extracts TE, TR, and TI,
        as well as scanning sequences;
        Runs the loaded classifier and returns the predicted analysis type 
        mapped to AnalysisType Enum
        """

        self.__download_dcm()
        dcm_dir = os.path.join('/tmp', self.scan.id, self.scan.id, 'DICOM')

        metadata = read_dcm_header(dcm_dir)
        if metadata == 1:
            print(f"Bad DICOM for {dcm_dir}, can't predict acquisition type")
            self.__cleanup()
            return(None)

        model_inputs = format_input_for_model(metadata)
        pred_scan_type = predict_scan_type(model, model_inputs)

        self.__cleanup()

        return(pred_scan_type)

        # dcm_header = self.scan.read_dicom()
        
        # try:
        #     ti = dcm_header.InversionTime
        # except AttributeError:
        #     ti = float(0.0)
        # try:
        #     n_echo = dcm_header.EchoNumbers
        # except AttributeError:
        #     n_echo = float(0.0)
        # try:
        #     te = dcm_header.EchoTime
        #     tr = dcm_header.RepetitionTime
        # except AttributeError:
        #     header = self.__use_dcm2niix_sidecar()
        #     te = float( header['EchoTime'] * 1000 )
        #     tr = float( header['RepetitionTime'] * 1000 )
        #     try:
        #         ti = header['InversionTime'] * 1000
        #     except KeyError:
        #         ti = float(0.0)

        # try:
        #     seq = dcm_header.ScanningSequence
        # except AttributeError:
        #     seq = []
        # scanning_sequences = {'SE':0, 'IR':0, 'GR':0, 'EP':0}
        # for sequence_name in scanning_sequences.keys():
        #     if sequence_name in seq:
        #         scanning_sequences[sequence_name] = 1

        # pred_analysis_type = self.clf.predict([[n_echo, te, tr, ti, 
        #                             scanning_sequences["SE"],
        #                             scanning_sequences["IR"],
        #                             scanning_sequences["GR"],
        #                             scanning_sequences["EP"]]])
        # return(AnalysisType(pred_analysis_type))

    def __use_dcm2niix_sidecar(self):
        """
        For DICOMs that place important info (TE, TR) in unusual or private tags;
        dcm2niix knows how to get to that, I don't;
        So we take the BIDS sidecar json that dcm2niix produces
        and use that for the DICOM header info
        """
        self.__download_dcm()
        converter = Dcm2niix()
        converter.inputs.source_dir = os.path.join('/tmp', self.scan.id, self.scan.id, 'DICOM')
        converter.inputs.output_dir = os.path.join('/tmp', self.scan.id)
        converter.run()

        bids_sidecar_file = glob('/tmp/' + self.scan.id + '/*.json')[0]
        with open(bids_sidecar_file, 'r') as bids_sidecar:
            dcm_header = json.load(bids_sidecar)

        shutil.rmtree( os.path.join('/tmp', self.scan.id) )

        return(dcm_header)

    def __download_dcm(self):
        """
        Downloads the DICOM to /tmp and unzips
        """
        self.scan.download( os.path.join('/tmp', self.scan.id + '.zip') )
        if os.path.isdir( os.path.join('/tmp', self.scan.id) ):
            os.mkdir( os.path.join('/tmp', self.scan.id) )
        with zipfile.ZipFile( os.path.join('/tmp', self.scan.id + '.zip') , 'r') as archive:
            archive.extractall( os.path.join('/tmp', self.scan.id) )
    
    def __cleanup(self):
        if os.path.isdir(os.path.join('/tmp', self.scan.id)):
            shutil.rmtree( os.path.join('/tmp', self.scan.id) )
        if os.path.exists(f"/tmp/{self.scan.id}.zip"):
            os.remove(f"/tmp/{self.scan.id}.zip")
