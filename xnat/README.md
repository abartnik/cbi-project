# CRANIA XNAT Client

--------------------

The CRANIA XNAT Client (CXC) is responsible for handling the automation of the broad workflow of a neuroimaging study. It may be helpful to think of the CXC as the experienced neuroimaging scientist as they review MRI exams, identifying the types of scans, assigning applicable analyses, and storing the exams within a greater project infrastructure. This is largely handled by integration with MRIO, the MRI Acquisition and Analysis Ontology, while individual scan classification is handled by a decision tree classifier trained to recognize common MRI acquisition parameters available in DICOM headers.

## How it works
The CXC continually reaches out to an XNAT server via XNAT's RESTful API (XAPI), checking for new exams as they come in from various studies.
The overall process is as follows:
  1) Establish a connection with the XNAT server
  2) Poll the XNAT server every five seconds for new exams in the prearchive
  3) Grab any new exams in prearchive
  4) Run the classifier on all scans in the exam and tag according to acquisition type
  5) Query MRIO for all analyses that accept the available scan types as input
  6) Pass the exam ID and name of analysis pipelines to be run to the Pipeline Engine API
