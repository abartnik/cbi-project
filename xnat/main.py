#!/usr/bin/env python3

import sys

# from attr import Attribute
sys.dont_write_bytecode = True
sys.path.append('.')

import os
import begin
import xnat
from xnat.exceptions import XNATValueError, XNATResponseError
from time import sleep
from urllib.request import urlopen
import requests
from requests.exceptions import ReadTimeout
from numpy import unique
from scan_classifier.ScanType import ScanType
from pydicom.errors import InvalidDicomError

from MriClassification import MriClassification
from assign_analyses import find_analyses, pipelines_map


# URL = "http://172.17.0.1:8080"
PASSWD = os.environ['AWS_XNAT_PASSWORD']
status = 0

scan_type_uris = {
                    ScanType.T2star: '390',
                    ScanType.DWI: '677',
                    ScanType.FLAIR: '385',
                    ScanType.fmap: '',
                    ScanType.rsfmri: '686',
                    ScanType.FSE: '388',
                    ScanType.MPRAGE: '392',
                    ScanType.PD: '387',
                    ScanType.SPGR: '392',
                }

@begin.start
def main(user_name: "Your XNAT username", 
         url: "The URL of the XNAT server", 
         project: "Project in XNAT to archive scans to",
         pipeline_engine_url="http://pipeline_engine:8100",
         test=None,
         storage_mode=False):
    """
    The entry point for the applicaton;
    Accepts XNAT username as input, with XNAT password coming from environment variables;
    Asks the XNAT server on five second interval if new scans have been sent into
    XNAT's prearchive; 
    If there are new scans, an external classifier is called to predict the analysis type
    for each scan; Each scan is then moved into the predicted XNAT project directory
    according to the predicted analysis type;
    Each analysis type is associated with pipeline comprised of docker containers
    that run on XNAT but are controlled here;
    """
    global username
    username = user_name
    global PASSWD
    PASSWD = PASSWD
    global URL
    URL = url

    print(f"The default XNAT Project is {project}")
    if storage_mode == True:
        print("Operating in storage mode, pipelines with not be started automatically...")
    else:
        ## So that we can skip over certain exams when archiving
        ## Consider finding a better way to do this
        nondefault_project_name = project

    ## Don't start the program until XNAT is available
    while True:

        try:
            status = urlopen(URL).getcode()

        except:
            print("Not up yet, trying again...")
            sleep(1)
            continue

        if status == 200:
            pass
        else:
            sleep(3)
            continue

        print("Connected!")
        break

    skip = []

    while True:
        ## Don't love creating a new session every loop, but XNAT doesn't update subjects across loops
        with xnat.connect(URL, user=username, password=PASSWD) as session:

            if test is not None:
                print("Testing... \n")

                scan_types = []
                scans_to_remove = []
                pipelines = []

                exit(0)

            if len(session.prearchive.find(status="READY")) > 0:

                for exam in session.prearchive.find(status="READY"):
                    examID = exam.name
                    print(exam.label)
                    if exam in skip:
                        continue

                    archived_scans = []
                    scan_types = []
                    scans_to_remove = []
                    pipelines = []
                    
                    for scan in exam.scans:

                        try:
                            print(exam.label)
                        except:
                            continue
                        
                        try:
                            header = scan.read_dicom()
                        except XNATResponseError:
                            print(f"Can't read dicom header for scan {scan.id}, skipping...")
                            continue
                        except IndexError:
                            print(f"Can't read dicom header for scan {scan.id}, skipping...")
                            continue
                        except InvalidDicomError:
                            print(f"Can't read dicom header for scan {scan.id}, skipping...")
                            continue
                        
                        ## Parse scans and prepare for passing to pipeline engine
                        if storage_mode == False:
                            print(scan.id)
                            series_description = str(header.SeriesDescription).lower()
                            if len(str(header.SeriesNumber)) > 2:
                                scans_to_remove.append(scan.id)
                                continue
                            elif 'asset' in series_description and 'cal' in series_description:
                                scans_to_remove.append(scan.id)
                                continue
                            elif 'localizer' in series_description:
                                scans_to_remove.append(scan.id)
                                continue

                            classifier = MriClassification(scan)
                            predicted_scan_type = classifier.predict_analysis_type()

                            if predicted_scan_type == None:
                                scans_to_remove.append(scan.id)
                                archived_scans.append([exam.subject, exam.name, scan.id, "unknown"])
                                continue
                            archived_scans.append([exam.subject, exam.name, scan.id, predicted_scan_type.name, header.SeriesTime])

                            if 'MPRAGE' in predicted_scan_type.name or 'SPGR' in predicted_scan_type.name or 'T1w' in predicted_scan_type.name:
                                try:
                                    ti = int(header.InversionTime)
                                    if ti == 0:
                                        scans_to_remove.append(scan.id)
                                        continue
                                except AttributeError:
                                    scans_to_remove.append(scan.id)
                                    continue
                                ## Gd_Classifier here
                                print(f"Likely SPGR or MPRAGE, would need to run Gd classifier on {scan.id}")
                                
                            if predicted_scan_type.name in ['SPGR', 'MPRAGE', 'rsfmri', 'FLAIR', 'DWI']:
                                if scan_type_uris[predicted_scan_type] in scan_types:
                                    same_types = [ scan for scan in archived_scans if predicted_scan_type.name == scan[3] and scan[2] not in scans_to_remove ]
                                    latest_time = None
                                    index = 0
                                    for i, dupe in enumerate(same_types):
                                        if latest_time is None or dupe[4] > latest_time:
                                            latest_time = dupe[4]
                                            index = i
                                    for i, dupe in enumerate(same_types):
                                        if i != index:
                                            scans_to_remove.append(dupe[2])
                                else:
                                    scan_types.append(scan_type_uris[predicted_scan_type])
                            print(" ")
                    
                        ## Set up DICOM routing rules for CBI projects in storage mode 
                        else:
                            patient_name = str(header.PatientName)

                            if "Weil_CardiacCT" in patient_name:
                                print("Weil_CardiacCT scan found...")
                                
                                nondefault_project_name = "Weil_CardiacCT"
                                patient_id = header.StudyID
                                
                                header.StudyDescription = nondefault_project_name
                                header.PatientID = patient_id
                                                                
                                if exam.project == "Unassigned":
                                    exam.move(nondefault_project_name)
                                    sleep(1)
                                
                                try:
                                    exam.archive(project = nondefault_project_name,
                                                 subject = patient_name,
                                                 experiment = patient_id,
                                                 overwrite='append')

                                except XNATResponseError:
                                    print("POST timed out, trying again...")
                                    continue

                                except ReadTimeout:
                                    print("Socket timed out, trying again...")
                                    continue

                                except XNATValueError:
                                    ## XNAT >=1.7.6 doesn't send the right response,
                                    ## which causes the python API to crash
                                    print(f"{patient_id} Archived with POST response error. Moving to the next step...")
                                    continue
                            
                            elif "Wilson_NeuroBones" in patient_name:
                                nondefault_project_name = "Wilson_NEURO_BONE"
                                patient_id = header.StudyID
                                
                                header.StudyDescription = nondefault_project_name
                                header.PatientID = patient_id
                                                                
                                if exam.project == "Unassigned":
                                    exam.move(nondefault_project_name)
                                    sleep(1)
                                
                                try:
                                    exam.archive(project = nondefault_project_name,
                                                 subject = patient_name,
                                                 experiment = patient_id,
                                                 overwrite='append')

                                except XNATResponseError:
                                    print("POST timed out, trying again...")
                                    continue

                                except ReadTimeout:
                                    print("Socket timed out, trying again...")
                                    continue

                                except XNATValueError:
                                    ## XNAT >=1.7.6 doesn't send the right response,
                                    ## which causes the python API to crash
                                    print(f"{patient_id} Archived with POST response error. Moving to the next step...")
                                    continue
                            
                            elif "PP" in patient_name:
                                nondefault_project_name = "Canty_ParaPET_2"
                                patient_id = header.StudyID
                                
                                header.StudyDescription = nondefault_project_name
                                header.PatientID = patient_id
                                                                
                                if exam.project == "Unassigned":
                                    exam.move(nondefault_project_name)
                                    sleep(1)
                                
                                try:
                                    exam.archive(project = nondefault_project_name,
                                                 subject = patient_name,
                                                 experiment = patient_id,
                                                 overwrite='append')

                                except XNATResponseError:
                                    print("POST timed out, trying again...")
                                    continue

                                except ReadTimeout:
                                    print("Socket timed out, trying again...")
                                    continue

                                except XNATValueError:
                                    ## XNAT >=1.7.6 doesn't send the right response,
                                    ## which causes the python API to crash
                                    print(f"{patient_id} Archived with POST response error. Moving to the next step...")
                                    continue
                            
                            
                            elif "Inglis_CDVRCT" in patient_name:
                                nondefault_project_name = "Inglis_CDVRCT"
                                patient_name = header.PatientID
                                patient_id = header.StudyID
                                
                                if exam.project == "Unassigned":
                                    exam.move(nondefault_project_name)
                                    sleep(1)
                                
                                try:
                                    exam.archive(project = nondefault_project_name,
                                                 subject = patient_name,
                                                 experiment = patient_id,
                                                 overwrite='append')

                                except XNATResponseError:
                                    print("POST timed out, trying again...")
                                    continue

                                except ReadTimeout:
                                    print("Socket timed out, trying again...")
                                    continue

                                except XNATValueError:
                                    ## XNAT >=1.7.6 doesn't send the right response,
                                    ## which causes the python API to crash
                                    print(f"{patient_id} Archived with POST response error. Moving to the next step...")
                                    continue
                            
                            else:
                                print("Scan for a CBI study found...")
                                
                                try:
                                    source_ae_title = header.file_meta.SourceApplicationEntityTitle
                                except AttributeError:
                                    source_ae_title = ""
                                    print("Source AE Title missing, proceeding without it...")
                                
                                ## Rules for ParaVision5/6
                                if "BRUKERPV" in source_ae_title:
                                    nondefault_project_name = str(header.PatientName)
                                    patient_name = header.PatientID
                                    patient_id = header.StudyID
                                    
                                    if "Siddiqui" in nondefault_project_name:
                                        patient_id = f"{str(patient_name)}_{str(patient_id)}"
                                    print(patient_id)
                                    
                                    if exam.project == "Unassigned":
                                        exam.move(nondefault_project_name)
                                        sleep(1)
                                    
                                    try:
                                        exam.archive(project = nondefault_project_name,
                                                    subject = patient_name,
                                                    experiment = patient_id,
                                                    overwrite='append')
                                        sleep(1)

                                    except XNATResponseError:
                                        print("POST timed out, trying again...")
                                        continue

                                    except ReadTimeout:
                                        print("Socket timed out, trying again...")
                                        continue

                                    except XNATValueError:
                                        ## XNAT >=1.7.6 doesn't send the right response,
                                        ## which causes the python API to crash
                                        print(f"{patient_id} Archived with POST response error. Moving to the next step...")
                                        continue

                                ## Rules for everything else
                                else:
                                    try:
                                        nondefault_project_name = header.StudyDescription
                                    except AttributeError:
                                        print(f"Study Description missing, check prearchive for {exam.label}")
                                        continue

                                    nondefault_project_name = header.StudyDescription
                                    patient_name = header.PatientID
                                    patient_id = header.StudyID
                                    
                                    if exam.project == "Unassigned":
                                        exam.move(nondefault_project_name)
                                        sleep(1)
                                    
                                    try:
                                        exam.archive(project = nondefault_project_name,
                                                     subject = patient_name,
                                                     experiment = patient_id,
                                                     overwrite='append')
                                        sleep(1)

                                    except XNATResponseError:
                                        print("POST timed out, trying again...")
                                        continue

                                    except ReadTimeout:
                                        print("Socket timed out, trying again...")
                                        continue

                                    except XNATValueError:
                                        ## XNAT >=1.7.6 doesn't send the right response,
                                        ## which causes the python API to crash
                                        print(f"{patient_id} Archived with POST response error. Moving to the next step...")
                                        continue

                    ## Assign analyses based on available scans
                    if storage_mode == False:
                        scan_types = unique(scan_types)
                        print(f"Scans to remove - ({len(scans_to_remove)}) - {scans_to_remove}")
                        print(f"Assigning analyses based on {len(scan_types)} available MRI types")
                        analyses = set([ analysis for analysis in  find_analyses(scan_types)['labels'] ])
                        for analysis in analyses:
                            try:
                                pipelines.append(pipelines_map[analysis])
                            except KeyError:
                                pass
                        if 'functional-connectome' in pipelines and 'smriprep' in pipelines:
                            pipelines.remove('smriprep')

                    # if examID not in skip:
                    #     print(f"No ({predicted_scan_type}) yet for {examID}!")
                    #     skip.append(examID)
                    #     continue

                    ## Archive scans into appropriate projeces
                    if storage_mode == False:
                        if exam.project == "Unassigned":
                            exam.move(project)
                            print(f"{examID} moved to {project} Project")
                            sleep(5)

                        try:
                            exam.archive(overwrite='append')
                            print(f"{examID} archived")

                        # For when contacting XNAT times out
                        except XNATResponseError:
                            print("POST timed out, trying again...")
                            continue

                        except ReadTimeout:
                            print("Socket timed out, trying again...")
                            continue

                        except XNATValueError:
                            ## XNAT >=1.7.6 doesn't send the right response,
                            ## which causes the python API to crash
                            print(f"{examID} Archived with POST response error. Moving to the next step...")
                            pass

                        if len(scan_types) == 0:
                            sleep(5)
                            continue

                    ## Launch pipelines
                    if storage_mode == False:
                        ## Find the subject we just archived so we can pass as object to pipeline manager
                        sleep(30)
                        with xnat.connect(URL, user=username, password=PASSWD) as temp_session:
                            session_data = temp_session.experiments[examID]
                            session_id = session_data.id
                            session_uri = session_data.fulluri
                        sleep(2)

                        for scan in archived_scans:
                            with xnat.connect(URL, user=username, password=PASSWD) as temp_session:
                                try:
                                    scan_data = temp_session.experiments[scan[1]].scans.filter(ID=str(scan[2]))[0]
                                    if scan[2] in scans_to_remove:
                                        scan_data.delete()
                                        print(f"Removed {scan[2]} from {session_id}")
                                        sleep(1)
                                    else:
                                        scan_data.type = scan[3]
                                        sleep(1)
                                except KeyError:
                                    print("Find some way of handling this")
                        for scan in scans_to_remove:
                            with xnat.connect(URL, user=username, password=PASSWD) as temp_session:
                                try:
                                    scan_data = temp_session.experiments[session_id].scans[scan]
                                    scan_data.delete()
                                    print(f"Removed scan {scan} from {session_id}")
                                except:
                                    print(f"Could not remove {scan}")
                                    pass

                        # pipelines = ['functional-connectome', 'sienax-t1', 'five_flair_measures']
                        for pipeline_name in pipelines:
                            requests.post(f'{pipeline_engine_url}/pipelines/{pipeline_name}', 
                                            json={'session_id': session_id, 'fulluri': session_uri})
                            print(f"Launched {pipeline_name}")
                            sleep(1)

            else:
                print("No new subjects")

        sleep(10)
