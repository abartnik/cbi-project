from fastapi import BackgroundTasks, FastAPI
from pydantic import BaseModel
from typing import Optional, List
import xnat
import os
import json
import yaml
from glob import glob
from time import sleep


app = FastAPI()

USER = os.environ['XNAT_USER']
PASSWD = os.environ['AWS_XNAT_PASSWORD']
URL = os.environ['XNAT_HOST']
CONTAINER_LIMIT = os.environ['CONTAINER_LIMIT']

#################################
## Classes for FastAPI objects ##
################################
class ScanSession(BaseModel):
    """
    Scanning session info obtained by CRANIA client.
    Contains ID and URI to coordinate launching a pipeline on that session
    """
    session_id: str
    fulluri: Optional[str]

class Pipeline(BaseModel):
    """
    Pipeline class used to handle the processes within a pipeline
    mapped to a scanning session
    """
    name: str
    processes: List[dict]
    session: ScanSession

class Container(BaseModel):
    """
    Contains info for a container to keep track of container status: 
    ('Complete', 'Running', 'Failed', etc.)
    to signify whether the next process in a pipeline should run or not
    """
    id: int
    created: Optional[str] = None
    timestamp: Optional[str] = None
    command_id: int
    docker_image: Optional[str] = None
    project: str
    status: str
    status_time: str
    workflow_id: str
    wrapper_id: int

try:
    app.container_limit = int(CONTAINER_LIMIT)
except:
    app.container_limit = 10
app.containers = []
app.pipelines = {}
for pipeline_file in glob(f"pipelines/*yml"):
    pipeline_name = pipeline_file.split('/')[-1].split('.')[0]
    with open(pipeline_file, 'r') as pipeline_config:
        app.pipelines[pipeline_name] = yaml.safe_load(pipeline_config)

#################################################
## Functions to be controlled by API endpoints ##
#################################################
def get_command_id(process_name) -> int:
    """
    Helper function that maps a command's wrapper name in XNAT to it's ID
    """
    with xnat.connect(URL, user=USER, password=PASSWD) as session:
        commands = session.get_json(URL + "/xapi/commands/")
        for command in commands:
            for wrapper in command['xnat']:
                if wrapper['name'] == process_name:
                    return(command['id'])

def running_containers():
    with xnat.connect(URL, user=USER, password=PASSWD) as session:
        return(len(session.get('/xapi/containers?nonfinalized=true').json()))

def wait_for_running_containers():
    print(app.container_limit, type(app.container_limit))
    while running_containers() > app.container_limit:
        sleep(60)

def launch_first_container(pipeline: dict, scan_session: dict):
    """
    Initiates the input pipeline on a specified scanning session
    """
    first_process_dict = pipeline['processes'][0]
    process_name = list(first_process_dict.keys())[0]
    first_process = first_process_dict[process_name]

    scan_session['fulluri'] = scan_session['fulluri'].replace('/data', '')
    first_process.update({'command_id': get_command_id(process_name)})
    query = first_process['query']
    ## TODO: handle URIs for scans vs sessions
    if 'SCAN' in query:
        command_url: str = f"/xapi/commands/{first_process['command_id']}/wrappers/{process_name}/root/scan/launch"
        query = query.replace('SCAN', scan_session['fulluri'])
    elif 'SESSION' in query:
        command_url: str = f"/xapi/commands/{first_process['command_id']}/wrappers/{process_name}/root/session/launch"
        query = query.replace("SESSION", scan_session['fulluri'])
    query = json.loads(query)

    wait_for_running_containers()
    with xnat.connect(URL, user=USER, password=PASSWD) as session:
        response = session.post(command_url, query=query)
    return(response)

def launch_next_container(previous_container: dict):
    """
    Accepts a completed container as input, and checks if there's another
    container that runs after it in all pipelines.
    Also checks for scan/session URIs to use as input for next container.
    Launches the next container.
    (consider refactoring)
    """
    for pipeline_name, pipeline in app.pipelines.items():
        for i, process in enumerate(pipeline['processes']):
            process_name = list(process.keys())[0]
            process.update({'command_id': get_command_id(process_name)})
            if process['command_id'] == previous_container['command_id']:
                try:
                    next_process = pipeline['processes'][i + 1]
                    next_process_name = list(next_process.keys())[0]
                except IndexError:
                    return({'pipeline': 'finished'})

                next_process.update({'command_id': get_command_id(next_process_name)})
                command_url = f"/xapi/commands/{next_process['command_id']}/wrappers/{next_process_name}/root/session/launch"

                with xnat.connect(URL, user=USER, password=PASSWD) as session:
                    container = session.get(f'/xapi/containers/{previous_container["id"]}').json()
                    for container_input in container['inputs']:
                        if container_input['type'] == 'raw' and container_input['name'] == 'session':
                            session_path = container_input['value']
                        elif container_input['type'] == 'raw' and container_input['name'] == 'scan':
                            scan_path = container_input['value']
                
                ## TODO: handle URIs for scans better
                query = next_process[next_process_name]['query']
                if 'SCAN' in query:
                    query = query.replace('SCAN', scan_path)
                elif 'SESSION' in query:
                    query = query.replace("SESSION", session_path)
                query = json.loads(query)

                wait_for_running_containers()
                with xnat.connect(URL, user=USER, password=PASSWD) as session:
                    response = session.post(command_url, query=query)
                # return(response.json()) # Don't need to return here since we want all pipelines with this process to run
    return({'Not in any pipelines': previous_container['command_id']})


######################################################
## Main app and definitions for endpoints and logic ##
######################################################
@app.post("/containers/{container_id}")
async def create_container(container: Container, container_id: int, background_tasks: BackgroundTasks):
    """
    Add new container to list, launch if completed
    """
    container_dict = container.dict()
    for processed_container in app.containers:
        if processed_container['id'] == container_id:
            return({'Already exists': container_id})
    app.containers.append(container_dict)
    if container_dict['status'] == "Complete":
                background_tasks.add_task(launch_next_container, container_dict)
                return({"launched": container_dict})
    return(container_dict)

@app.put("/containers/{container_id}")
async def update_container(container: Container, container_id: int, background_tasks: BackgroundTasks):
    """
    Update the status of an existing container, launch if completed
    """
    container_dict = container.dict()
    for processed_container in app.containers:
        if processed_container['id'] == container_id:
            processed_container.update({'timestamp': container_dict['timestamp'],
                                        'status': container_dict['status'],
                                        'status_time': container_dict['status_time']})
            if container_dict['status'] == "Complete":
                background_tasks.add_task(launch_next_container, container_dict)
                return({"launched_next": container_dict})
            return(container_dict)
    return({"Not found": container_id})

@app.get("/containers/{container_id}")
async def get_container(container_id: int):
    """
    Return container info if in containers list
    """
    for container in app.containers:
        if container['id'] == container_id:
            return(container)
    return({"Not found": container_id})

@app.get("/containers/")
async def get_containers():
    """
    Return all containers info
    """
    return {'containers': app.containers}

@app.post("/pipelines/{pipeline_name}")
async def launch_pipeline(scan_session: ScanSession, pipeline_name: str, background_tasks: BackgroundTasks):
    """
    Accepts pipeline name and scanning session JSON in body,
    launches the named pipeline for the specified session
    """
    pipeline_dict = app.pipelines[pipeline_name]
    scan_session_dict = scan_session.dict()
    print(pipeline_dict)
    background_tasks.add_task(launch_first_container, pipeline_dict, scan_session_dict)
    return({"launched": pipeline_dict})

@app.get("/pipelines/{pipeline_name}")
async def get_pipeline(pipeline_name: str):
    """
    Return pipeline and processes
    """
    try:
        pipeline = app.pipelines[pipeline_name]
        return(pipeline)
    except KeyError:
        return({"Not found": pipeline_name})

@app.get("/pipelines")
async def get_pipelines():
    """
    Return all pipelines with processes
    """
    return({'pipelines': app.pipelines})

@app.get("/refresh/pipelines")
async def refresh_pipelines():
    """
    Completely rebuild pipeline configs based on YAML files
    """
    app.pipelines = {}
    for pipeline_file in glob(f"pipelines/*yml"):
        pipeline_name = pipeline_file.split('/')[-1].split('.')[0]
        with open(pipeline_file, 'r') as pipeline_config:
            app.pipelines[pipeline_name] = yaml.safe_load(pipeline_config)
    return(app.pipelines)

@app.get("/update/pipelines")
async def update_pipelines():
    """
    Look for new pipeline config YAML files and add to current collection
    """
    for pipeline_file in glob(f"pipelines/*yml"):
        pipeline_name = pipeline_file.split('/')[-1].split('.')[0]
        if pipeline_name not in list(app.pipelines['pipelines'].keys()):
            with open(pipeline_file, 'r') as pipeline_config:
                app.pipelines[pipeline_name] = yaml.safe_load(pipeline_config)
    return(app.pipelines)

@app.get("/trim_containers")
async def trim_container_list():
    """
    Trim the list of containers to free up memory
    """
    if len(app.containers) > 50:
        for i in range(len(app.containers) - 50):
            app.containers.pop(0)
    return({"containers": app.containers})

@app.get("/containers/limit/{limit}")
async def update_container_limit(limit: int):
    """
    Set a new limit for maximum concurrent processes
    """
    try:
        limit = int(limit)
    except:
        limit = os.environ['CONTAINER_LIMIT']
    app.container_limit = limit
    return({"container_limit": app.container_limit})
