# Pipeline Engine API

----------------------

The CRANIA Pipeline Engine API, built on [FastAPI](https://github.com/tiangolo/fastapi), is responsible for coordinating how the XNAT Container Service runs analyses in predefined pipelines. A pipeline is any ordered set of analysis processes where the output of the preceding step is the input for the next step. Individual analyses are handled by the [XNAT Container Service](https://wiki.xnat.org/container-service/), which mounts scans and sessions from XNAT into docker containers and runs prespecified commands on the mounted data. The Container Service also handles adding the results of analyses back into XNAT automatically. Fortunately, results of analyses are added back in consistent ways, which the Pipeline Engine can take advantage of when chaining outputs -> inputs for processing steps in a pipeline.

## OpenAPI endpoint documentation
FastAPI follows the [OpenAPI specification](https://swagger.io/specification/) and automatically creates a documentation endpoint for the Pipeline Engine API. This can be found at [http://localhost:8100/docs](http://localhost:8100/docs) of the host running the Pipeline Engine server and provides semantic documentation and examples of usage for all endpoints and classes defined in the Pipeline Engine API.

## Launching a pipeline
A pipeline is launched via a POST request to `/pipelines/{pipeline_name}` with attached JSON data bearing information on the session to process, where `pipeline_name` matches the name of a pipeline defined in a [pipeline config file](#pipeline-configs).

An example session JSON is shown below:

```javascript
{
    'session_id': 'CBI_XNAT_E000123',
    'fulluri': '/archive/experiments/CBI_XNAT_E000123'
}
```

The Pipeline Engine creates a dictionary containing the pipeline's processes as defined in the pipeline's config file and calls `launch_first_container(pipeline: dict, scan_session: dict)` to start the pipeline. Because FastAPI is asynchronous, multiple pipelines may be run in paralell if another POST request is sent to `/pipelines/{pipeline_name}` while the first pipeline is still running.

### Launching the next container
Each container process that gets launched is added to an internal list of dictionaries, where each dictionary encodes the name, ID, and runtime status of the container. This list is referenced with each POST/PUT request to spawn or update a container process. If a container's status is marked as 'Complete' the Pipeline Engine calls `launch_next_container(previous_container: dict)`, which finds the next process defined in the processes list for the pipeline and the mount point on the XNAT server for the output of the previous container. This mount point is then used as the URI for the mountpoint of the input for the next container.

## Pipeline configs
Pipelines and their processes are defined in pipeline config YAML files, stored in `./pipelines`. This makes it easy to chain analyses together and create new pipelines in a human-readable format. The basic structure of a pipeline config comprises the `pipeline_name` and a list of `processes`, where each process name corresponds to the name of a command defined in the XNAT Container Service. Each process bears a generic `data_type` and `query` used to send a POST request to the Container Service to launch a container.

An example of a pipeline config YAML is shown below:
```yaml
pipeline_name: "Functional Connectome"

processes:
 - fmriprep:
     data_type: "imageSessionData"
     query: '{"session": "SESSION"}'
 - generate-functional-connectomes:
     data_type: "imageSessionData"
     query: '{"session": "SESSION"}'
 - bctpy-correlation:
     data_type: "imageSessionData"
     query: '{"session": "SESSION"}'
```

A `data_type` may be 'imageSessionData' or 'imageScanData' according to data types defined in the XNAT schema. A process that uploads data (called "assessors" by XNAT) directly into the XNAT database must use 'imageSessionData' as XNAT specifically defines these new data as extensions of a session in its schema.

A `query` may map to "SESSION" or "SCAN" depending on the `data_type` used. The functions that the Pipeline Engine uses to launch new containers will search for "SESSION" and "SCAN" in a query and replace them with the correct URI of the session or scan that it is working with. This also an area of future development to further refine the process.
